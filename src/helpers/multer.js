const multer = require('multer');
const mimetype = require('mime-types');

const createStorage = (path) => {
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            console.log("file", file);
            cb(null, path)
        },
        filename: (req, file, cb) => {
            const ext = mimetype.extension(file.mimetype);
            if(ext !== 'pdf'){
                cb(null, `${file.fieldname}${Date.now()}.${ext}`)
                console.log("asdasda");
            }else{
                const fileError = new Error("El formato no es aceptado por el servidor");
                cb(fileError, null);
            }
        }
    });
    return storage;
}

module.exports = createStorage;