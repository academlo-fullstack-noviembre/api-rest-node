const nodemailer = require('nodemailer');
const googleapis = require('googleapis');
const fs = require('fs');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');

const Oauth2 = googleapis.google.auth.OAuth2;

const createTransporter = async () => {
    const oauthClient = new Oauth2(
        process.env.G_CLIENT_ID,
        process.env.G_CLIENT_SECRET,
        'https://developers.google.com/oauthplayground'
    );

    oauthClient.setCredentials({refresh_token: process.env.G_REFRESH_TOKEN});

    try{
        const accessToken = await oauthClient.getAccessToken();
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: process.env.G_USER,
                accessToken,
                clientId: process.env.G_CLIENT_ID,
                clientSecret: process.env.G_CLIENT_SECRET,
                refreshToken: process.env.G_REFRESH_TOKEN
            }
        });

        return transporter;
    }catch(error){
        console.log(error);
    }
};



const sendEmail = async (options) => {
    const handlebarsOptions = {
        viewEngine: {
            extName: ".handlebars",
            partialsDir: path.resolve('./src/views'),
            defaultLayout: false,
        },
        viewPath: path.resolve('./src/views'),
    };

    try{
        const gmailTransporter = await createTransporter();
        gmailTransporter.use('compile', hbs(handlebarsOptions))
        const results = await gmailTransporter.sendMail(options);
        console.log(results);
    }catch(error){
        console.log(error);
    }
}

const emailOptions = {
    subject: "Probando el envío de correos",
    to: "oscar.islas@academlo.com",
    from: process.env.G_USER
}

module.exports = {
    sendEmail,
    emailOptions
}