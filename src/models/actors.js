'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Actors extends Model {
    static associate(models) {
      // define association here
    }
  };
  Actors.init({
    firstname: {
      type: DataTypes.STRING,
      validate: {
        not: "<script[\d\D]*?>[\d\D]*?</script>", //regex
        notEmpty: true,
        isAlpha: true,
        len: [0,50]
      }
    },
    lastname: DataTypes.STRING,
    dob: {
      type: DataTypes.DATEONLY,
      validate: {
        isDate: true
      }
    },
    biography: DataTypes.TEXT,
    profile_photo: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Actors',
    underscored: true
  });
  return Actors;
};