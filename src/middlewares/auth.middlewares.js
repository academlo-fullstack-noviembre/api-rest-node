const jwt = require('jsonwebtoken');

const validateToken = (req, res, next) => {
    //Obtener el token
    let token = req.headers["authorization"];
    
    if(token){
        //Quitamos la palabra Bearer del valor de la cabecera authorization
        token = token.replace(/^Bearer\s+/, "");

        const decoded = jwt.verify(token, process.env.SECRET_KEY);

        if(!decoded) {
            req.decoded = decoded;
            console.log("decoded", decoded);
            return next(req, res);            
        }
        return res.status(401).json({message: "El token es invalido"});
    }
    return res.status(401).json({message: "El token es invalido"});
}

module.exports = {validateToken};