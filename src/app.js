const express = require('express');
const app = express();
const logger = require("morgan");
const fs = require('fs');
const multer = require('multer');
const createStorage = require('./helpers/multer');
const swaggerUi = require('swagger-ui-express');
const swaggerDocs = require('../swagger.json');
const {emailOptions, sendEmail} = require('./helpers/nodemailer');
const {validateToken} = require('./middlewares/auth.middlewares');
require('dotenv').config();

const storage = createStorage("./uploads");

//Almacen personalizado con un tamaño limite de 1mb por archivo
const upload = multer({storage: storage, limits: {fileSize: 1000000}});

//Routes
const actorsRoutes = require('./routes/actors.routes');
const directorsRoutes = require('./routes/directors.routes');

//Middleware
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.use(logger('combined', {stream: fs.createWriteStream('./access.log', {flags: 'a'})}));
app.set('views', '/src/views/');
app.use(express.json());

app.get("/", (req, res) => res.json({"academlo-api": "1.0.0"}));

//Rutas publicas
app.post("/api/v1/gallery", upload.single('image'), (req, res) => {
    try{
        res.send(req.file);
    }catch(error){
        console.log(JSON.stringify(error));
        res.status(400).json({message: error.message});
    }
});

//Rutas protegidas
app.use("/api/v1/", actorsRoutes);
app.use("/api/v1/", directorsRoutes);


/* === Envío de correos === */
app.post("/api/v1/reset-password", (req, res) => {
    emailOptions.subject = "Restablecer contraseña";
    emailOptions['template'] = 'email' //res.render('email', )
    emailOptions['context'] = { title: 'Restablecer contraseña' } //res.render('email', {title: 'Restablecer contraseña'} );
    /*
    emailOptions = {
        subject: "Restablecer contraseña",
        to: "oscar.islas@academlo.com",
        from: process.env.G_USER,
        template: 'email',
        context: { title: 'Restablecer contraseña' }
    }
    */
    sendEmail(emailOptions)
});





//Middleware para manejar errores
app.use((err, req, res, next) => {
    if(err.name === "SequelizeValidationError"){
        const errObj = {};
        err.errors.map( er => {
            errObj[er.path] = er.message;
        })
        return res.status(400).send(errObj);
    }
    return res.status(500).send('Ups tenemos un problema en el servidor, intentalo más tarde!');
});

module.exports = app;