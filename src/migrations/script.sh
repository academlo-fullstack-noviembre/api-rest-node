sequelize-cli model:generate --name Actors --attributes firstname:string,lastname:string,dob:date,biographt:text,profile_photo:string,active:boolean

sequelize-cli model:generate --name Directors --attributes firstname:string,lastname:string,dob:date,biographt:text,profile_photo:string,active:boolean

sequelize-cli model:generate --name Genres --attributes name:string,active:boolean

sequelize-cli model:generate --name Contents --attributes title:string,description:text,total_seasons:integer,imdb_score:decimal,relase_date:string,play_time:integer,imdb_link:string,active:boolean

sequelize-cli model:generate --name ContentGenres --attributes genre_id:integer,content_id:integer,active:boolean

sequelize-cli model:generate --name ContentActors --attributes actor_id:integer,content_id:integer,active:boolean

sequelize-cli model:generate --name ContentDirectors --attributes director_id:integer,content_id:integer,active:boolean

