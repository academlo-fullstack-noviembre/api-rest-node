'use strict';

const today = new Date();

const actors = [
  {
    firstname: "Will",
    lastname: "Smith",
    dob: "1968-09-25",
    biography: "Willard Carroll Smith, Jr.​​, ​​ más conocido como Will Smith, es un actor, rapero, productor de cine, productor de televisión y productor discográfico estadounidense.",
    profile_photo: "https://spoiler.bolavip.com/__export/1594763139104/sites/bolavip/img/2020/07/14/will_smith_crop1594763138495.jpg_423682103.jpg",
    active: true,
    created_at: today,
    updated_at: today
  }
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('actors', actors, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('actors', null, {});
  }
};
