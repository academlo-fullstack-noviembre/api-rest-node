const {create} = require("../src/controllers/actors.controller");
const supertest = require("supertest");
const app = require('../src/app');
const {Actors} = require("../src/models");

let id = 0;

test("Probando la creación de un actor", async(done) => {
    //AAA

    //1. Arrange
    const today = new Date();

    const actor = {
        firstname: "Will",
        lastname: "Smith",
        dob: "1968-09-25",
        biography: "Willard Carroll Smith, Jr.​​, ​​ más conocido como Will Smith, es un actor, rapero, productor de cine, productor de televisión y productor discográfico estadounidense.",
        profile_photo: "https://spoiler.bolavip.com/__export/1594763139104/sites/bolavip/img/2020/07/14/will_smith_crop1594763138495.jpg_423682103.jpg",
        active: true,
        created_at: today,
        updated_at: today
    };

    //2. Act
    let response = await supertest(app).post("/api/v1/actors").send(actor);
    id = response.body.id;
    //3. Assert
    //status
    //propiedad del objeto creado
    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty("active", true);
    expect(response.body).toHaveProperty("id");
    expect(response.body).toHaveProperty("firstname", "Will");
    done();
});

afterAll(async () => {
    await Actors.destroy({where: {id}});
});